﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using System.Data.SqlClient;
using System.Data.SQLite;
using System.Data.Common;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;

using MebiusLib;

namespace DB_Decryptor
{
    public partial class SearchUtility : Form
    {
        public SearchUtility()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Used to select the database we are going to process
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDatabaseFileDalog_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            // might add more extentions, but this is only intended to sqlite for now
            openFileDialog1.Filter = "db Files (.db)|*.db|All Files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;

            openFileDialog1.Multiselect = false;

            DialogResult result = openFileDialog1.ShowDialog();

            if (result == DialogResult.OK)
            {
                txtDatabaseFilePath.Text = openFileDialog1.FileName;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            string databaseFilePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "0000000000000001 2.db");

            if (!string.IsNullOrEmpty(txtDatabaseFilePath.Text))
            {
                databaseFilePath = txtDatabaseFilePath.Text;
            }

            if (string.IsNullOrEmpty(databaseFilePath))
            {
                txtSearchResults.Text = "Database not specified";
                return;
            }

            try
            {
                using (SQLiteConnection con = new SQLiteConnection(
                    string.Format("Data Source={0};Version=3;",
                    databaseFilePath)))
                {
                    con.Open();

                    using (SQLiteCommand cmd = con.CreateCommand())
                    {
                        // should always be called BACKUP, but may need to add as an option
                        cmd.CommandText = @"select * from Backup";
                        cmd.CommandType = CommandType.Text;

                        SQLiteDataReader r = cmd.ExecuteReader();

                        SJCLDecryptor decryptor;

                        string destPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.GetFileName(databaseFilePath) + " - Export.txt");
                        Int32 count = 1; // used to match with database row
                        StringBuilder sb = new StringBuilder();
                        StringBuilder sbExport = new StringBuilder();

                        while (r.Read())
                        {
                            string user = r["user"].ToString();
                            string password = "password";
                            string result = string.Empty;
                            // the admin password is always...password
                            // otherwise, we need the user's password
                            if (user == "8") //admin account
                            {
                                password = "password";
                            }
                            else
                            {
                                password = txtUserPassword.Text;
                            }

                            try
                            {
                                // the sjcl library needs the label to be wrapped 
                                // in quotes
                                string json = r["value"].ToString()
                                    .Replace("{iv:\"", "{\"iv\":\"")
                                    .Replace("\",salt:\"", "\",\"salt\":\"")
                                    .Replace("\",ct:\"", "\",\"ct\":\"");
                                decryptor = new SJCLDecryptor(json, password);
                                result = decryptor.Plaintext;

                                sbExport.AppendLine(count.ToString() + " : " + Environment.NewLine + result + Environment.NewLine);
                            }
                            catch (Exception ex)
                            {
                                if (chkIncludeExceptions.Checked)
                                {
                                    sb.AppendLine(count.ToString() + " : " + ex.ToString() + Environment.NewLine);
                                }
                            }
                            finally
                            {
                                count++; // so we can keep track of the line number
                            }
                        }

                        if (sbExport.Length > -1)
                        {
                            File.WriteAllText(destPath, sbExport.ToString());
                        }

                        if (sb.Length > -1)
                        {
                            txtSearchResults.Visible = true;
                            lblSearchResults.Visible = true;
                            txtSearchResults.Text = sb.ToString();
                        }
                        else if (!string.IsNullOrEmpty(txtSearchString.Text))
                        {
                            txtSearchResults.Text = "Search string not found :(";
                        }
                        else
                        {
                            txtSearchResults.Visible = false;
                            lblSearchResults.Visible = false;
                        }
                    }
                }
            }
            catch (Exception exDatabase)
            {
                //probably a database issue
                txtSearchResults.Text = exDatabase.ToString();
            }
        }
        
        /// <summary>
        /// this takes all the options and does the work by reading the database
        /// decrypting it, and searching for the phrase
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            string databaseFilePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),"0000000000000001 2.db");

            if (!string.IsNullOrEmpty(txtDatabaseFilePath.Text))
            {
                databaseFilePath = txtDatabaseFilePath.Text;
            }

            if (string.IsNullOrEmpty(databaseFilePath))
            {
                txtSearchResults.Text = "Database not specified";
                    return;
            }

            try
            {
                using (SQLiteConnection con = new SQLiteConnection(
                    string.Format("Data Source={0};Version=3;",
                    databaseFilePath)))
                {
                    con.Open();

                    using (SQLiteCommand cmd = con.CreateCommand())
                    {
                        // should always be called BACKUP, but may need to add as an option
                        cmd.CommandText = @"select * from Backup";
                        cmd.CommandType = CommandType.Text;

                        SQLiteDataReader r = cmd.ExecuteReader();

                        SJCLDecryptor decryptor;

                        Int32 count = 1; // used to match with database row
                        StringBuilder sb = new StringBuilder();

                        while (r.Read())
                        {
                            string user = r["user"].ToString();
                            string password = "password";
                            string result = string.Empty;
                            // the admin password is always...password
                            // otherwise, we need the user's password
                            if (user == "8") //admin account
                            {
                                password = "password";
                            }
                            else
                            {
                                password = txtUserPassword.Text;
                            }

                            try
                            {
                                // the sjcl library needs the label to be wrapped 
                                // in quotes
                                string json = r["value"].ToString()
                                    .Replace("{iv:\"", "{\"iv\":\"")
                                    .Replace("\",salt:\"", "\",\"salt\":\"")
                                    .Replace("\",ct:\"", "\",\"ct\":\"");
                                decryptor = new SJCLDecryptor(json, password);
                                result = decryptor.Plaintext;

                                if (!string.IsNullOrEmpty(txtSearchString.Text))
                                {
                                    string searchString = txtSearchString.Text;
                                    if (result.IndexOf(searchString, 0, StringComparison.OrdinalIgnoreCase) > -1)
                                    {
                                        sb.AppendLine("FOUND - " + count.ToString() + " : " + result.Substring(0, 100) + Environment.NewLine);
                                    }
                                }

                                // count++; // so we can keep track of the line number
                            }
                            catch (Exception ex)
                            {
                                if (chkIncludeExceptions.Checked)
                                {
                                    sb.AppendLine(count.ToString() + " : " + ex.ToString() + Environment.NewLine);
                                }
                            }
                            finally
                            {
                                count++; // so we can keep track of the line number
                            }
                            if (sb.Length > -1)
                            {
                                txtSearchResults.Visible = true;
                                lblSearchResults.Visible = true;
                                txtSearchResults.Text = sb.ToString();
                            }
                            else if (!string.IsNullOrEmpty(txtSearchString.Text))
                            {
                                txtSearchResults.Text = "Search string not found :(";
                            }
                            else
                            {
                                txtSearchResults.Visible = false;
                                lblSearchResults.Visible = false;
                            }
                        }
                    }
                }
            }
            catch (Exception exDatabase)
            {
                //probably a database issue
                txtSearchResults.Text = exDatabase.ToString();
            }

        }

        /// <summary>
        /// Attempting to use .net security libraries to decrypt (NOT USED)
        /// </summary>
        /// <param name="input"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public string DecryptText(string input, string password)
        {
            // Get the bytes of the string
            byte[] bytesToBeDecrypted = Convert.FromBase64String(input);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            byte[] bytesDecrypted = AES_Decrypt(bytesToBeDecrypted, passwordBytes);

            string result = Encoding.UTF8.GetString(bytesDecrypted);

            return result;
        }

        /// <summary>
        /// Attempting to use .net security libraries to decrypt (NOT USED)
        /// </summary>
        /// <param name="bytesToBeDecrypted"></param>
        /// <param name="passwordBytes"></param>
        /// <returns></returns>
        public byte[] AES_Decrypt(byte[] bytesToBeDecrypted, byte[] passwordBytes)
        {
            byte[] decryptedBytes = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                //using (RijndaelManaged AES = new RijndaelManaged())
                using (AesManaged AES = new AesManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                        cs.Close();
                    }
                    decryptedBytes = ms.ToArray();
                }
            }

            return decryptedBytes;
        }
    }
}
