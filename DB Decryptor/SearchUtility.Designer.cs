﻿namespace DB_Decryptor
{
    partial class SearchUtility
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSearchSearch = new System.Windows.Forms.Button();
            this.txtSearchString = new System.Windows.Forms.TextBox();
            this.lblSearchString = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDatabaseFilePath = new System.Windows.Forms.TextBox();
            this.btnDatabaseFileDalog = new System.Windows.Forms.Button();
            this.txtSearchResults = new System.Windows.Forms.TextBox();
            this.lblSearchResults = new System.Windows.Forms.Label();
            this.lblUserPassword = new System.Windows.Forms.Label();
            this.txtUserPassword = new System.Windows.Forms.TextBox();
            this.btnExport = new System.Windows.Forms.Button();
            this.chkIncludeExceptions = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // btnSearchSearch
            // 
            this.btnSearchSearch.Location = new System.Drawing.Point(200, 284);
            this.btnSearchSearch.Name = "btnSearchSearch";
            this.btnSearchSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearchSearch.TabIndex = 0;
            this.btnSearchSearch.Text = "&Search";
            this.btnSearchSearch.UseVisualStyleBackColor = true;
            this.btnSearchSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtSearchString
            // 
            this.txtSearchString.Location = new System.Drawing.Point(9, 26);
            this.txtSearchString.Name = "txtSearchString";
            this.txtSearchString.Size = new System.Drawing.Size(259, 20);
            this.txtSearchString.TabIndex = 1;
            // 
            // lblSearchString
            // 
            this.lblSearchString.AutoSize = true;
            this.lblSearchString.Location = new System.Drawing.Point(9, 9);
            this.lblSearchString.Name = "lblSearchString";
            this.lblSearchString.Size = new System.Drawing.Size(78, 13);
            this.lblSearchString.TabIndex = 2;
            this.lblSearchString.Text = "S&earch string...";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Database file...";
            // 
            // txtDatabaseFilePath
            // 
            this.txtDatabaseFilePath.Location = new System.Drawing.Point(9, 67);
            this.txtDatabaseFilePath.Name = "txtDatabaseFilePath";
            this.txtDatabaseFilePath.Size = new System.Drawing.Size(232, 20);
            this.txtDatabaseFilePath.TabIndex = 4;
            // 
            // btnDatabaseFileDalog
            // 
            this.btnDatabaseFileDalog.Location = new System.Drawing.Point(250, 65);
            this.btnDatabaseFileDalog.Name = "btnDatabaseFileDalog";
            this.btnDatabaseFileDalog.Size = new System.Drawing.Size(25, 20);
            this.btnDatabaseFileDalog.TabIndex = 5;
            this.btnDatabaseFileDalog.Text = "...";
            this.btnDatabaseFileDalog.UseVisualStyleBackColor = true;
            this.btnDatabaseFileDalog.Click += new System.EventHandler(this.btnDatabaseFileDalog_Click);
            // 
            // txtSearchResults
            // 
            this.txtSearchResults.Location = new System.Drawing.Point(12, 151);
            this.txtSearchResults.Multiline = true;
            this.txtSearchResults.Name = "txtSearchResults";
            this.txtSearchResults.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtSearchResults.Size = new System.Drawing.Size(259, 107);
            this.txtSearchResults.TabIndex = 6;
            this.txtSearchResults.Visible = false;
            // 
            // lblSearchResults
            // 
            this.lblSearchResults.AutoSize = true;
            this.lblSearchResults.Location = new System.Drawing.Point(9, 132);
            this.lblSearchResults.Name = "lblSearchResults";
            this.lblSearchResults.Size = new System.Drawing.Size(88, 13);
            this.lblSearchResults.TabIndex = 7;
            this.lblSearchResults.Text = "Search Results...";
            this.lblSearchResults.Visible = false;
            // 
            // lblUserPassword
            // 
            this.lblUserPassword.AutoSize = true;
            this.lblUserPassword.Location = new System.Drawing.Point(9, 91);
            this.lblUserPassword.Name = "lblUserPassword";
            this.lblUserPassword.Size = new System.Drawing.Size(86, 13);
            this.lblUserPassword.TabIndex = 9;
            this.lblUserPassword.Text = "User &password...";
            // 
            // txtUserPassword
            // 
            this.txtUserPassword.Location = new System.Drawing.Point(9, 108);
            this.txtUserPassword.Name = "txtUserPassword";
            this.txtUserPassword.Size = new System.Drawing.Size(259, 20);
            this.txtUserPassword.TabIndex = 8;
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(118, 284);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(75, 23);
            this.btnExport.TabIndex = 10;
            this.btnExport.Text = "&Export";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // chkIncludeExceptions
            // 
            this.chkIncludeExceptions.AutoSize = true;
            this.chkIncludeExceptions.Checked = true;
            this.chkIncludeExceptions.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIncludeExceptions.Location = new System.Drawing.Point(13, 263);
            this.chkIncludeExceptions.Name = "chkIncludeExceptions";
            this.chkIncludeExceptions.Size = new System.Drawing.Size(102, 17);
            this.chkIncludeExceptions.TabIndex = 11;
            this.chkIncludeExceptions.Text = "Print Exceptions";
            this.chkIncludeExceptions.UseVisualStyleBackColor = true;
            // 
            // SearchUtility
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 310);
            this.Controls.Add(this.chkIncludeExceptions);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.lblUserPassword);
            this.Controls.Add(this.txtUserPassword);
            this.Controls.Add(this.lblSearchResults);
            this.Controls.Add(this.txtSearchResults);
            this.Controls.Add(this.btnDatabaseFileDalog);
            this.Controls.Add(this.txtDatabaseFilePath);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblSearchString);
            this.Controls.Add(this.txtSearchString);
            this.Controls.Add(this.btnSearchSearch);
            this.Name = "SearchUtility";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSearchSearch;
        private System.Windows.Forms.TextBox txtSearchString;
        private System.Windows.Forms.Label lblSearchString;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDatabaseFilePath;
        private System.Windows.Forms.Button btnDatabaseFileDalog;
        private System.Windows.Forms.TextBox txtSearchResults;
        private System.Windows.Forms.Label lblSearchResults;
        private System.Windows.Forms.Label lblUserPassword;
        private System.Windows.Forms.TextBox txtUserPassword;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.CheckBox chkIncludeExceptions;
    }
}

